git clone --recursive https://github.com/boostorg/boost

cd boost
./bootstrap.sh
./b2 --prefix="${CTE_TP_BIN}/boost-trunk" install

ln -s ${CTE_TP_BIN}/include/boost ${CTE_TP_INCL}/
ls -s ${CTE_TP_BIN}/lib | xargs -I{} ln -s ${CTE_TP_BIN}/lib/{} ${CTE_TP_LIB}/
